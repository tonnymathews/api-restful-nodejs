const express = require("express");
const ProductCtrl = require("../controllers/ProductController");

const Router = express.Router();

Router.get("/", ProductCtrl.index) // api.com/product
  .post("/", ProductCtrl.create) // api.com/product/create: Crear nuevo registro
  .get("/:key/:value", ProductCtrl.find, ProductCtrl.show) // api.com/product/category/Hogar Show: Muestra un registro especifico
  .put("/:key/:value", ProductCtrl.find, ProductCtrl.update) // api.com/product/name/MotoG Update: Actualiza producto especifico
  .delete("/:key/:value", ProductCtrl.find, ProductCtrl.remove); // api.com/product/name/MotoG

module.exports = Router;
