module.exports = {
  PORT: process.env.PORT || 3077,
  DB: process.env.DB || "mongodb://localhost:27017/api-rest-node"
};
